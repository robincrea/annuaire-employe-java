package com.crea.dev1.poo.devclasses;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Iterator;
import java.util.Vector;

import com.crea.dev1.poo.beans.Employe;

//Cette classe permet de créer un fichier structuré d'employés é partir
//d'informations placées dans un vecteur
//Elle propose également des méthodes pour lire et afficher é l'écran, les
//enregistrements du fichier
public class Annuaire {
	private Vector<Employe> collectionEmployes;
	private RandomAccessFile fichierAnnuaire;
	private int nbEmployes;

	// constructeurs
	public Annuaire(Vector<Employe> cemp, RandomAccessFile rf) {
		collectionEmployes = cemp;
		fichierAnnuaire = rf;
		nbEmployes = 0;
	}

	public Annuaire(Vector<Employe> cemp) {
		collectionEmployes = cemp;
		nbEmployes = 0;
		try {
			fichierAnnuaire = new RandomAccessFile("annuaire.dat", "rw");
		} catch (IOException e) {
			System.out.println("PB création fichier annuaire ds Constructeur" + e.getMessage());
		}
	}
	// méthode privée pour obtenir une chaine de longueur fixe.
	// complémenter avec des espaces si la longueur de la chaine
	// est inférieure é la longueur souhaitée
	private String formatChaine(String chaine, int longueur) {
		String chaineRes;
		// on tronque si la taille de la chaine dépasse la
		// longueur indiquée
		if (chaine.length() >= longueur) {
			chaineRes = chaine.substring(0, longueur);
		} else {
			StringBuffer sb = new StringBuffer(chaine);
			// on complémente avec des espaces
			for (int i = 0; i < (longueur - chaine.length()); i++) {
				sb.append(' ');
			} // fin for
			chaineRes = sb.toString();
		} // fin else
		return chaineRes;
	}// fin formatChaine

	// recopie de la collection des employés dans le fichier structuré
	public void creerAnnuaire() throws IOException {
		Iterator<Employe> it = collectionEmployes.iterator();
		while (it.hasNext()) {
			Employe emp = (Employe) it.next();
			// écriture dans fichierAnnuaire
			fichierAnnuaire.writeInt(emp.getNumero());
			fichierAnnuaire.writeChars(formatChaine(emp.getNom(), 20));
			fichierAnnuaire.writeChars(formatChaine(emp.getAdresse(), 40));
			fichierAnnuaire.writeChars(formatChaine(emp.getTelephone(), 20));
			fichierAnnuaire.writeDouble(emp.getSalaire());
			nbEmployes++;
		} // fin fin while
		System.out.println("Annuaire créé avec succés !!!!");
	}// fin creerAnnuaire

	// méthode de lecture d'une chaine de caractére ds fichierAnnuaire
	//
	private String lireChaine(int nbcars) throws IOException {
		String chaine = "";
		for (int i = 0; i < nbcars; i++) {
			char c = fichierAnnuaire.readChar();
			chaine += c;
		}
		return chaine;
	}
	// lecture d'un employe dans le fichier annuaire
	private Employe lireUnEmploye(int pos) throws IOException {
		Employe emp = new Employe();
		System.out.println("lecture de l'enregistrement é la position " + pos);
		fichierAnnuaire.seek(pos * 172);
		emp.setNumero(fichierAnnuaire.readInt());
		emp.setNom(lireChaine(20));
		emp.setAdresse(lireChaine(40));
		emp.setTelephone(lireChaine(20));
		emp.setSalaire(fichierAnnuaire.readDouble());

		return emp;
	}// fin lireUnEmploye

	// affichage d'un employeé
	public void afficherEmploye(int pos) throws IOException {
		Employe emp = lireUnEmploye(pos);
		emp.afficher();
	}

	// nombre d'employés ds l'annuaire
	public int getNbEmploye() {
		return nbEmployes;
	}
}