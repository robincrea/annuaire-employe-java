package com.crea.dev1.poo.test;
import com.crea.dev1.poo.beans.Employe;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.crea.dev1.poo.dao.EmployeDAO;

class EmployeDAOTest {

	Employe ref1 = new Employe(1,"Taha Ridene", "Rue mvc 2 97400 troisTiers", "022000000", 12000);
	Employe ref2 = new Employe(2,"Robin Ferrari","Route de frontenex 51 1207 Genève", "0763822274", 12000);
	
	@BeforeEach
	public void testAdd() {
		// Ajouter les deux références et vérifier si la DAO retourne "true"
		assertTrue(EmployeDAO.add(this.ref1));
		assertTrue(EmployeDAO.add(this.ref2));
		
		// Ajout d'un utilisateur existant dans la base de donnée
		// Pour vérifier que celui-ci est bien refusé par Mysql
		assertFalse(EmployeDAO.add(this.ref2));

    }
	@AfterEach
    public void testDelete() {
		// Supprimer les deux références et vérifier si la DAO retourne "true"
		assertTrue(EmployeDAO.delete(ref1.getNumero()));
		assertTrue(EmployeDAO.delete(ref2.getNumero()));
		// Suppression d'un utilisateur non-existant dans la base de donnée
		// Pour vérifier que MYSQL retourne bien false
		assertFalse(EmployeDAO.delete(ref1.getNumero()));
    }
	

	@Test
	void testAddAndDelete() {
		// ICI on a pas besoin de faire de test 
		// vu qu'ils sont executé dans le After et Before each
		// Si il ne passe pas aucun autre test ne passera
		assertTrue(true);
	}
	
	@Test
	void testGetByNumero(){

		Employe userget = EmployeDAO.getByNumero(ref1.getNumero());
		assertEquals(userget.getNom(), ref1.getNom());
		assertEquals(userget.getAdresse(), ref1.getAdresse());
		assertEquals(userget.getSalaire(), ref1.getSalaire());
		
	}
	
	@Test
	void testUpdateAdresseEmployeByNum() {
		String adresse = "Rue du test 22";
		EmployeDAO.updateAdresse(ref2.getNumero(), adresse);
		assertEquals(adresse, EmployeDAO.getByNumero(ref2.getNumero()).getAdresse());
	}
	
	@Test
	void testUpdateSalaire() {
		Double salaire = 13343.0;
		EmployeDAO.updateSalaire(ref2.getNumero(), salaire);
		Double newSalaire =  EmployeDAO.getByNumero(ref2.getNumero()).getSalaire();
		assertEquals(salaire,newSalaire, 0.001);
		
	}
	@Test
	void testUpdateNom() {
		String nom = "Jean";
		EmployeDAO.updateNom(ref2.getNumero(), nom);
		String newNom =  EmployeDAO.getByNumero(ref2.getNumero()).getNom();
		assertEquals(nom,newNom);
	}
	
	@Test
	void testUpdateTelephone() {
		String phone = "0228848382";
		EmployeDAO.updateTelephone(ref2.getNumero(), phone);
		String newPhone =  EmployeDAO.getByNumero(ref2.getNumero()).getTelephone();
		assertEquals(phone,newPhone);
	}
	
	

	

}
