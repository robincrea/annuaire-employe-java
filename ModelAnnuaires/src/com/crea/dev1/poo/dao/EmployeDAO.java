package com.crea.dev1.poo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.crea.dev1.poo.beans.Employe;
import com.crea.dev1.poo.utils.DBAction;

public class EmployeDAO {
	
	public static boolean add(Employe employe) {
		String numero = Integer.toString(employe.getNumero());
		String salaire = Double.toString(employe.getSalaire());

		String employeValues = "'"+numero+"','"+employe.getNom()+ "','"+employe.getAdresse()+ "','"+employe.getTelephone()+ "','"+salaire+"'";
		
		// On prépare notre requete SQL
		String req = "INSERT INTO `employes` (`numero`, `nom`, `adresse`, `telephone`, `salaire`) VALUES ("+employeValues+")";
		
		
		try {
			// DEMMARER LA CONNECTION
			DBAction.DBConnexion();
			DBAction.getStm().executeUpdate(req) ;
			System.out.println("L'employe: " + employe.getNom() + " a bien été ajouté");
			return true;
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("L'employe: " + employe.getNom() + " n'a pas pu être ajouté");
			// e.printStackTrace();
			return false;
		}
		
	}
	
	
	/**
	 * Met à jour un employer par son numéro
	 * @param num
	 * @param adresse
	 * @return
	 * @throws SQLException
	 */
	private static int update(String num, String column, String value) throws SQLException {
		
		System.out.println(value);
		int result = -1;
		DBAction.DBConnexion();
		String req = "UPDATE employes SET "+column+" = '"+value+"' WHERE numero='"+num+"' ";
		result = DBAction.getStm().executeUpdate(req);
		DBAction.DBClose();
		return result;
	}
	

	/**
	 * Met à jour l'adresse d'un employe par son numéro
	 * @param num
	 * @param adresse
	 * @return
	 * @throws SQLException
	 */
	public static int updateAdresse(int num, String adresse){
		System.out.println(adresse);
		try {
			return EmployeDAO.update( Integer.toString(num), "adresse", adresse);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * Met à jour le salaire d'un employe par son numéro
	 * @param num
	 * @param salaire
	 * @return
	 * @throws SQLException
	 */
	public static int updateSalaire(int num, Double salaire){


		try {
			return EmployeDAO.update( String.valueOf(num), "salaire", Double.toString(salaire));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * Met à jour le telephone d'un employe par son numéro
	 * @param num
	 * @param telephone
	 * @return
	 * @throws SQLException
	 */
	public static int updateTelephone(int num, String telephone){
		try {
			return EmployeDAO.update( String.valueOf(num), "telephone", telephone);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * Met à jour le nom d'un employe par son numéro
	 * @param num
	 * @param nom
	 * @return
	 * @throws SQLException
	 */
	public static int updateNom(int num, String nom){
		try {
			return EmployeDAO.update( String.valueOf(num), "nom", nom);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	
	/**
	 * Met à jour le numero d'un employe par son numéro
	 * @param num
	 * @param newNum
	 * @return
	 * @throws SQLException
	 */
	public static int updateNumero(int num, int newNum){
		try {
			return EmployeDAO.update( String.valueOf(num), "num", String.valueOf(newNum));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	
	/**
	 * Suppression d'un employe de la base de donnée par son numero
	 * @param numero int
	 * @return boolean
	 */
	public static boolean delete(int num) {		
		String numero = Integer.toString(num);
		try {
			// On prépare notre requete SQL
			String req = "DELETE FROM employes WHERE employes.numero = '"+numero+"' ";
		
			// DEMMARER LA CONNECTION
			DBAction.DBConnexion();
			
			// Execution de la requete et init
			if(DBAction.getStm().executeUpdate(req) > 0) {
				System.out.println("L'employe avec numero: " + numero +" a bien été supprimé");
				return true;
			}else{
				System.out.println("L'employe avec le numero: " + numero +" n'existe pas ou ne peut pas être supprimé");
				return false;
			}
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}


	
	public static Employe getByNumero(int numero){
		// Comme notre méthode getBy ne prend que des String (Car ensuite requete SQL type String)
		// Nous allons convertir notre numeo de type int en type String
		return EmployeDAO.getOneBy("numero", Integer.toString(numero));
	}
	
	public static Employe getByNumero(String numero){
		// Comme notre méthode getBy ne prend que des String (Car ensuite requete SQL type String)
		// Nous allons convertir notre numeo de type int en type String
		return EmployeDAO.getOneBy("numero", numero);
	}

	
	/**
	 * Récupère un arraylist contenant les livre d'une colonne donnée correspondant
	 * à une valeur donné
	 * 
	 * @param column : Nom de la colonne dans la bdd
	 * @param value
	 * @return
	 */
	private static ArrayList<Employe> getBy(String column, String value) {

		// Initialisation d'une variable employes qui contiendera les résultat de notre
		// requête
		ArrayList<Employe> employes = new ArrayList<Employe>();

		// On prépare notre requete SQL
		String req = "SELECT * FROM employes WHERE employes." + column + "='" + value + "'";

		try {
			// DEMMARER LA CONNECTION
			DBAction.DBConnexion();
			// Exécution de la requête
			DBAction.setRes(DBAction.getStm().executeQuery(req));

			// Boucle sur chacun des résultat de la requette
			while (DBAction.getRes().next()) {
				// Assignation dea valeur retourné par un des résultat dans un objet de type
				// Employe
				Employe elevTemp = EmployeDAO.getFromRes(DBAction.getRes());

				// Ajout de l'employe dans le tableau d'employe
				employes.add(elevTemp);
			}

			// Stopper la connection
			DBAction.DBClose();

		} catch (SQLException e) {
			// Afficage de la trace en cas d'erreur
			e.printStackTrace();
		}

		// retour des résultat
		return employes;
	}

	public static Employe getOneBy(String column, String value) {
		// Exécute la méthode getBy et retourne uniquement le 1er résultat
		return EmployeDAO.getBy(column, value).get(0);

	}
	
	/**
	 * retourne tous les employe dans un arraylist
	 * @return ArrayList<Employe> : Liste des employes
	 * @throws SQLException
	 */
	public static ArrayList<Employe> getAll(){
		
	    ArrayList<Employe> employes = new ArrayList<Employe>();

	    // On prépare notre requete SQL dans la variable req
		String req = "SELECT * FROM employes";
	
		// On démarre la connexion au serveur SQL
		DBAction.DBConnexion();
		
		// Execution de la requete 
		try {
			DBAction.setRes(DBAction.getStm().executeQuery(req));
			while(DBAction.getRes().next()) {
				Employe employeTemp = EmployeDAO.getFromRes(DBAction.getRes());
				employes.add(employeTemp);
			}
			// Stopper la connection
			DBAction.DBClose();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return employes;
								
	}


	private static Employe getFromRes(ResultSet res) throws SQLException {
		Employe employe = new Employe();
		employe.setNumero(res.getInt(1));
		employe.setNom(res.getString(2));
		employe.setAdresse(res.getString(3));
		employe.setTelephone(res.getString(4));
		employe.setSalaire(res.getDouble(5));

		return employe;
	}

}
