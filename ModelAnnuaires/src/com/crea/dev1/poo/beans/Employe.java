package com.crea.dev1.poo.beans;

public class Employe {
	private int numero;
	private String nom;
	private String adresse;
	private String telephone;
	private double salaire;

	// Constructeurs
	public Employe(){		
	}
	
	// Constructeur
	public Employe(int numero, String nom, String adresse, String telephone, double salaire) {
		this.numero = numero;
		this.nom = nom;
		this.adresse = adresse;
		this.telephone = telephone;
		this.salaire = salaire;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}

//Autres m�thodes
	public void afficher() {
		System.out.println("Numéro : " + numero);
		System.out.println("Nom du client : " + nom);
		System.out.println("Adresse du client : " + adresse);
		System.out.println("Téléphone du client : " + telephone);
		System.out.println("Salaire : " + salaire);
	}
}