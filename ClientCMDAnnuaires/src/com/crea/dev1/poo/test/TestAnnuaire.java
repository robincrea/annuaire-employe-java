package com.crea.dev1.poo.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Vector;

import com.crea.dev1.poo.devclasses.Annuaire;
import com.crea.dev1.poo.beans.Employe;

public class TestAnnuaire {

	public static void main(String[] args) {
		try {
			// création d'un flux en lecture écriture pour le ficher annauaire.dat
			RandomAccessFile rf = new RandomAccessFile("annuaire.dat", "rw");

			// creation de la collection des employés
			Vector<Employe> cemp = new Vector<Employe>();
			// remplissage du vecteur avec des données en dur (pour l'instant ....)
			Employe e1 = new Employe(1, "Dodet", "Rue des saintspéres", "0166902354", 1000.0);
			Employe e2 = new Employe(2, "Prince", "Rue de Paris", "0166901254", 2389.0);
			Employe e3 = new Employe(3, "Jackson", "Rue des	volontaires", "0176902354", 7854);
			Employe e4 = new Employe(4, "Madone", "Rue petit", "0266902354", 1000.0);
			Employe e5 = new Employe(5, "Chavalier", "Rue des saintspéres", "01992354", 2309.0);
			cemp.add(e1);
			cemp.add(e2);
			cemp.add(e3);
			cemp.add(e4);
			cemp.add(e5);
			Annuaire annuaire = new Annuaire(cemp, rf);
			annuaire.creerAnnuaire();
			System.out.println("Nombre Total d'employés ds l'annuaire : " + annuaire.getNbEmploye());
			annuaire.afficherEmploye(0);
			annuaire.afficherEmploye(1);
			annuaire.afficherEmploye(2);
		} catch (FileNotFoundException e) {
			System.out.println("Ds main TestAnnuaire : " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Ds main TestAnnuaire : " + e.getMessage());
		}
	}// fin main
}
