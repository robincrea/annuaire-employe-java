# Installation

1. récupration de la base de donnée dans (dumps->annuaire.sql)
2. modification de l'accès a la base de donnée dans `ModelAnnuaires->com.crea.dev1.poo.utils.DBAction.java`
3. Pour plus d'info sur la configuration de eclipse [Cliquez Ici](https://gitlab.com/robincrea/easy-mvc-j2e/blob/master/docs/README.md)