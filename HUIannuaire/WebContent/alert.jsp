<jsp:useBean id="alert" class="com.crea.dev1.jee.hui.utils.Alert" scope="request" />  
<jsp:setProperty name="alert" property="*" />

<% if(alert.isDisplay()){ %>
	<div class="container mt-4 mb-1">
		<div class="alert alert-<%= alert.getType() %>" role="alert">
		 <%= alert.getMessage() %>
		</div>
	</div>
<% } %>
