<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.poo.beans.Employe"%> 
<% ArrayList<Employe> employes = (ArrayList<Employe>) request.getAttribute("employes"); %>

 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Liste des employes"/>
 </jsp:include>
  <jsp:include page="../../alert.jsp" />
 

	<div class="container mt-5">
	<div class="d-flex justify-content-between align-items-center">
		<h1>Liste des employes</h1>
		<a class="btn btn-primary" href="${pageContext.request.contextPath}/employes/add">Ajouter</a>
	</div>
		<table class="table table-striped">
	  		<thead>
	  			<tr>
	  				<th>Num</th>
	  				<th>Nom</th>
	  				<th>telephone</th>
	  				<th>salaire</th>
	  				<th>Adresse</th>
	  				<th class="text-right">Actions</th>
	  			</tr>
	  		</thead>
	  		<tbody>
	        <%
			for(Employe e:employes){%> 
	        <%-- Arranging data in tabular form 
	        --%> 
	            <tr> 
	            	<td><%= e.getNumero() %></td>
	                <td>
	                	<a href="${pageContext.request.contextPath}/employes/show/<%= e.getNumero() %>">
	                		<%= e.getNom() %>
	                	</a>
	                </td> 
	                <td><%= e.getTelephone() %></td>
	                <td><%= e.getSalaire() %></td> 
	                <td><%= e.getAdresse() %></td> 
	                <td class="text-right">
	                	<a class="btn btn-success" href="${pageContext.request.contextPath}/employes/show/<%= e.getNumero() %>">
	                		Afficher
	                	</a>
	                	<a class="btn btn-primary" href="${pageContext.request.contextPath}/employes/edit/<%= e.getNumero() %>">
	                		Modifier
	                	</a>
	                	
	                	<a class="btn btn-danger" href="${pageContext.request.contextPath}/EmployeController?action=delete&numero=<%= e.getNumero() %>">
	                		Supprimer
	                	</a>
	                </td>
	            </tr> 
	            <%}%> 
	        </tbody>
	      </table>
      </div>


 <jsp:include page="../../footer.jsp" />