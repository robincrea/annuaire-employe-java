<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:useBean id="employe"
	class="com.crea.dev1.poo.beans.Employe" scope="request" />
<jsp:setProperty name="employe" property="*" /> 
<%@page import="java.util.ArrayList"%>

 
 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Ajouter un &eacute;l&egrave;ve"/>
 </jsp:include>
 
 <jsp:include page="../../alert.jsp" />

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-md-6">
			<form method="POST" action="${pageContext.request.contextPath}/EmployeController?action=create">

			 
			 <div class="form-group">
			   <label for="numero">Numéro d'employe</label>
			   <input type="number" name="numero" class="form-control" id="numero" required>
			 </div>

			 
			 <div class="form-group">
			   <label for="adresse">Nom de l'employe</label>
			   <input type="text" name="nom" class="form-control" id="nom" required>
			 </div>
			 
			 <div class="form-group">
			   <label for="telephone">telephone de l'employe</label>
			   <input type="text" name="telephone" class="form-control" id="telephone" required>
			 </div>
			 
			 <div class="form-group">
			   <label for="salaire">salaire de l'employe</label>
			   <input type="number" name="salaire" class="form-control" id="salaire" required>
			 </div>
			 
			 
			 <div class="form-group">
			   <label for="adresse">adresse de l'employe</label>
			   <input type="text" name="adresse" class="form-control" id="adresse" required>
			 </div>
			 
			 <input class="btn btn-primary" type="submit">
			 
			 
			</form>
		</div>
	</div>
</div>

<jsp:include page="../../footer.jsp" />

