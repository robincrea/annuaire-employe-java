<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="employe" class="com.crea.dev1.poo.beans.Employe" scope="request" />  
<jsp:setProperty name="employe" property="*" />   
 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Afficher ${param.eleve.getNom()}"/>
 </jsp:include>

<div class="container">
	<div class="row justify-content-md-center mt-5">
		<div class="col-md-8">
			Numero: <jsp:getProperty property="numero" name="employe" /><br/>
			Nom: <jsp:getProperty property="nom" name="employe" /><br/>
			Salaire: <jsp:getProperty property="salaire" name="employe" /><br/>
			Adresse: <jsp:getProperty property="adresse" name="employe" /><br/>
			Telephone: <jsp:getProperty property="telephone" name="employe" /><br/>
		</div>
	</div>
</div>
 <jsp:include page="../../footer.jsp" />