<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="employe"
	class="com.crea.dev1.poo.beans.Employe" scope="request" />
<jsp:setProperty name="employe" property="*" />  

<%@page import="java.util.ArrayList"%>
 
 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Modifier un &eacute;l&egrave;ve"/>
 </jsp:include>

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-md-6">
			<form method="POST" action="${pageContext.request.contextPath}/EmployeController?action=update">

			 
			 <div class="form-group">
			   <label for="num">Numéro de l'employe</label>
			   <input type="text" name="numeroRef" value="<%= employe.getNumero() %>" class="form-control" id="num" disabled>
			   <input type="text" name="numero" value="<%= employe.getNumero() %>" class="form-control" id="num" hidden>
			 </div>
			 
			
   		
			 
			 <div class="form-group">
			   <label for="adresse">Nom de l'employe</label>
			   <input type="text" name="nom" value="<%= employe.getNom() %>" class="form-control" id="nom" required>
			 </div>
			 
			 <div class="form-group">
			   <label for="telephone">Téléphone</label>
			   <input type="text" name="telephone" value="<%= employe.getTelephone() %>" class="form-control" id="telephone" required>
			 </div>
			 
			 
			 <div class="form-group">
			   <label for="adresse">adresse de l'employe</label>
			   <input type="text" name="adresse" value="<%= employe.getAdresse() %>" class="form-control" id="adresse" required>
			 </div>
			 
			  
			 <div class="form-group">
			   <label for="salaire">Salaire de l'employe</label>
			   <input type="text" name="salaire" value="<%= employe.getSalaire() %>" class="form-control" id="salaire" required>
			 </div>
			 
			 <input class="btn btn-primary" type="submit">
			 
			 
			</form>
		</div>
	</div>
</div>

<jsp:include page="../../footer.jsp" />
