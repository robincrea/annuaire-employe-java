package com.crea.dev1.jee.hui.utils;

import java.util.Arrays;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

public class RequestUtils {
	 /**
     * Méthode permettant de récupérer l'action et la valeur associé à cette action de l'url.
  
     * Par exemple: Pour http://localhost:8080/InterfaceEasyJEE/users/edit/AGUE001
     * nous obtiendront un tableau avec à l'index 0 "edit" et a l'index 1 "AGUE001"
     * 
     * @param request
     * @return String[]: [0]: action [1]: actionValue
     */
    public static String[] getActionRequest(HttpServletRequest request) {
     	
    	// Définition de l'action par défault et des autre variables
    	String action = "index";
    	String actionValue = null;
    	String[] returnValue = new String[2];
    	
    	// Récupération de l'url du projet
    	// request.getRequestURI() retournera l'url complete p.ex(localhost:8080/HUIannuaire/employe/add)
    	// request.getContextPath() retournera l'url du point d'entrée (p.ex: localhost:8080/HUIannuaire/)
    	// Nous allons supprimer l'url du point d'entrée de notre url complete
    	// Nous aurons ainsi uniquement le "path" 
    	// Le path est la partie de l'url qui nous intéresse (p.ex: employes/add/ )
		String path = request.getRequestURI().substring(request.getContextPath().length() + 1);

		// Convertion du path (p.ex /employes/edit/1) en tableau de string
		String[] pathParts = path.split("/");
		Integer nbPathParts = pathParts.length; 
		
	

		
		if(nbPathParts == 2) {
			//p.ex:  /employes/add (/1/2)
			action = pathParts[1];
		}
		if(nbPathParts == 3 ) {
			// /employes/edit/1
			action = pathParts[1];
			actionValue = pathParts[2];
		}
		
		if(request.getParameter("action") != null) {
			action = request.getParameter("action");
		}
		returnValue[0] = action;
		returnValue[1] = actionValue;

		System.out.println("action: " + action);		
		System.out.println("actionValue: " + actionValue);

		return returnValue;
    }
    
    /**
     * Méthode pour définir la timezone sur UTC afin d'éviter les bug de date
     * Je sais que ce n'est surement pas la bonne méthode mais c'est celle que j'ai trouvé de plus simple
     * On peu ensuite jouer avec Simpledateformat et sa méthode setTimeZone pour changer l'heure
     */
    public static void defineTimeZone() {
        //Get default time zone
        TimeZone timeZone = TimeZone.getDefault();
        System.out.println("1. Default time zone is:");
        System.out.println(timeZone);

        //Setting UTC time zone as default time zone
        TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
        TimeZone.setDefault(utcTimeZone);

    }
    
    

}
