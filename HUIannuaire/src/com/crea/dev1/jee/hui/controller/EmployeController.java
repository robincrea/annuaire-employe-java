package com.crea.dev1.jee.hui.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crea.dev1.jee.hui.utils.Alert;
import com.crea.dev1.jee.hui.utils.RequestUtils;
import com.crea.dev1.poo.beans.Employe;
import com.crea.dev1.poo.dao.EmployeDAO;

/**
 * Servlet implementation class EmployeController
 */
@WebServlet("/EmployeController")
public class EmployeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeController() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
            // ICI VIENDRA LE TRAITEMENT DE NOS REQUETES
    		System.out.println("test");
    		// Récupération de le l'action et sa valeur associé via l'url
    	   	String[] actionRequest = RequestUtils.getActionRequest(request);
    		String action = actionRequest[0];
    		String actionValue = actionRequest[1];

    		
    		// Initialisation du forward
    		String forward = null;
    		
    		// déclaration des variable
    		
    		String nom;
    		Integer numero;
    		Double salaire;
    		String adresse;
    		String telephone;
    		boolean success;
    		Alert alert = new Alert();
    		Employe etemp;

    		// Répartition des actions
    		switch(action) {
    			case("index"):
    				System.out.println("Liste des employes");
    				ArrayList<Employe> users = EmployeDAO.getAll();
    				System.out.println(users);
    				request.setAttribute("employes", users);
    				forward = "index.jsp";
    				break;
    			case("show"):
    				etemp = EmployeDAO.getByNumero(actionValue);

    				request.setAttribute("employe", etemp);
    				forward = "show.jsp";
    				break;
    			case("add"):
    				forward = "add.jsp";
    				break;
    			case("edit"):
        			numero = Integer.parseInt(actionValue);
        			
    				if(actionValue.contentEquals("") || actionValue.equals(null)) {
    					// TODO Log error recupération
    				}else {
    					 etemp = EmployeDAO.getByNumero(numero);
    					if(etemp.equals(null)) {
    						forward = "error.jsp";
    	
    					}else {
    						request.setAttribute("employe", etemp);
    						forward = "edit.jsp";
    					}
    				}
        			break;

    			case("create"):
    				numero = Integer.parseInt(request.getParameter("numero"));
    				nom = request.getParameter("nom");
    				adresse = request.getParameter("adresse");
    				telephone = request.getParameter("telephone");
    				salaire = Double.parseDouble(request.getParameter("salaire"));
    				
        			Employe newEmploye = new Employe(numero, nom, adresse, telephone, salaire);
        			
        			success = EmployeDAO.add(newEmploye);
        			if(success) {
        				alert = new Alert("addSuccess", nom);
        			}else {
        				alert = new Alert("addError", nom);

        			}
        			request.setAttribute("employes", EmployeDAO.getAll());
    				forward = "index.jsp";
    				
        			break;
    			case("delete"):
        			// TODO
        			success = EmployeDAO.delete(Integer.parseInt(request.getParameter("numero")));
    	    		if(success) {
    	    		    alert = new Alert("deleteSuccess", request.getParameter("numero"));
    				}else {
    	    		    alert = new Alert("deleteError", request.getParameter("numero"));
    				}
    	    		request.setAttribute("employes", EmployeDAO.getAll());
    				forward = "index.jsp";
    			
        			break;
    			case("update"):
        			
    				numero = Integer.parseInt(request.getParameter("numero"));
					nom = request.getParameter("nom");
					adresse = request.getParameter("adresse");
					telephone = request.getParameter("telephone");
					salaire = Double.parseDouble(request.getParameter("salaire"));
					
					
        			EmployeDAO.updateAdresse(numero, adresse);
        			EmployeDAO.updateNom(numero, nom);
        			EmployeDAO.updateSalaire(numero, salaire);
        			EmployeDAO.updateTelephone(numero, telephone);
    				
        			alert = new Alert("updateSuccess", nom);

        			
        			request.setAttribute("employes", EmployeDAO.getAll());
        			forward = "index.jsp";
        			break;

    			default:
    				forward = "404.jsp";
    				break;
    		}
    		
    		request.setAttribute("alert", alert);


    		// Affichage de la jsp correspondante à l'action demandée avec RequestDispatcher
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/views/employes/"+forward);
    		dispatcher.forward(request, response);


    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		   try {
		        processRequest(request, response);
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
   


}
